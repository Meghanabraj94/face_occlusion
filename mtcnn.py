"""Performs face alignment and stores face thumbnails in the output directory."""
# MIT License
# 
# Copyright (c) 2016 David Sandberg
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from scipy import misc
import sys
import os
import argparse
import tensorflow as tf
import numpy as np
import cv2
#import facenet

import random
from time import sleep

def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = os.path.dirname(__file__)
#lib_path = os.path.join(this_dir)
add_path(this_dir)
import detect_face
import face_preprocess


from time import time
_tstart_stack = []
def tic():
    _tstart_stack.append(time())
def toc(fmt="Elapsed: %s s"):
    print(fmt % (time()-_tstart_stack.pop()))

def bgr2rgb(bgr_img):
    """
    convert opencv read image(bgr) to rgb image
    """
    return bgr_img[:,:,::-1]

def rgb2bgr(rgb_img):
    """
    convert rgb image to opencv bgr format
    """
    return rgb_img[:,:,::-1]

def grey2rgb(grey_img):
    w, h = grey_img.shape
    ret = np.empty((w, h, 3), dtype=np.uint8)
    ret[:, :, 0] = ret[:, :, 1] = ret[:, :, 2] = img
    return ret

def draw_boxes(img, boxes):

    for b in boxes:
        cv2.rectangle(img, (int(b[0]), int(b[1])), (int(b[2]), int(b[3])), (0, 255, 0))
        #print(b)
    return img

def draw_points(img, points):

    for p in points.T: # T?
        for i in range(5):
            cv2.circle(img, (p[i], p[i + 5]), 1, (0, 0, 255), 2)
    return img

# simple mtcnn wrapper

class mtcnn(object):
    """
    This class represents a multi face detection in a single image by mtcnn.

    Parameters
    ----------
    minsize : int
        minimum face size for detection
    threshold : array_like, float
        three steps's threshold in format [ s1, s2, s3 ]
    factor : float
        scale factor
    gpu_memory_fraction : float
        gpu memeory faraction shared for this Tensorflow model
    Attributes
    ----------
    tfconfig : TF class
        config for tensorflow session.
    pnet, rnet, onet : tf models
        models for mtcnn.
    session : tf class
        session to run the model.

    """
    def __init__(self, minsize=40, gpu_memory_fraction=0.9):
        self.tfconfig = tf.ConfigProto(allow_soft_placement=True, log_device_placement=False)
        self.tfconfig.gpu_options.allow_growth = True
        self.tfconfig.gpu_options.per_process_gpu_memory_fraction = gpu_memory_fraction
        self.session = tf.Session(graph=None, config=self.tfconfig)

        self.pnet, self.rnet, self.onet = detect_face.create_mtcnn(self.session, None)
        
        self.minsize = minsize # minimum size of face
        self.threshold = [ 0.6, 0.7, 0.9 ]  # three steps's threshold
        self.factor = 0.709 # scale factor
        '''
        for other alignment setting 
        minsize = 20
        threshold = [0.6,0.7,0.7]
        factor = 0.85
        '''

        print("mtcnn detector loaded sucessfully")

    def __call__(self, RGB_img):
        tic()
        bounding_boxes, points = detect_face.detect_face(RGB_img, self.minsize,
                                                        self.pnet, self.rnet, self.onet,
                                                        self.threshold, self.factor)
        toc()
        print("num of faces being detected : {}".format(bounding_boxes.shape[0]))
        return bounding_boxes, points

def create_mtcnn(minsize=40, gpu_memory_fraction=0.9):
    #mtcnn_detector = mtcnn(minsize, gpu_memory_fraction)
    return mtcnn(minsize, gpu_memory_fraction)


#class mtcnn_aligner(object):


def align_image1(mtcnn_detector, rgb_img):
    bounding_boxes, points = mtcnn_detector(rgb_img)
    nrof_faces = bounding_boxes.shape[0]
    _bbox = None
    _landmark = None
    if nrof_faces>0:
        det = bounding_boxes[:,0:4]
        img_size = np.asarray(rgb_img.shape)[0:2]
        bindex = 0
        if nrof_faces>1:
            # following is to find the face in the middle if multi face detected in one image
            bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
            img_center = img_size / 2
            offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
            offset_dist_squared = np.sum(np.power(offsets,2.0),0)
            bindex = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
        _bbox = bounding_boxes[bindex, 0:4]
        _landmark = points[:, bindex].reshape( (2,5) ).T
    else:
        print("no face was detected")

    warped = face_preprocess.preprocess(rgb_img, bbox=_bbox, landmark = _landmark, image_size=args.image_size)
    bgr = warped[...,::-1]
    #print(bgr.shape)
    if target_file.endswith('.png'):
        cv2.imwrite(target_file, bgr, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
    else:
        cv2.imwrite(target_file, bgr, [int(cv2.IMWRITE_JPEG_QUALITY), 100])





def align_image(bounding_boxes, points, rgb_img, savepath):
    #bounding_boxes, points = mtcnn_detector(rgb_img)
    nrof_faces = bounding_boxes.shape[0]
    _bbox = None
    _landmark = None
    if nrof_faces>0:
        det = bounding_boxes[:,0:4]
        img_size = np.asarray(rgb_img.shape)[0:2]
        bindex = 0
        if nrof_faces>1:
            # following is to find the face in the middle if multi face detected in one image
            bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
            img_center = img_size / 2
            offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
            offset_dist_squared = np.sum(np.power(offsets,2.0),0)
            bindex = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
        _bbox = bounding_boxes[bindex, 0:4]
        _landmark = points[:, bindex].reshape( (2,5) ).T


        warped = face_preprocess.preprocess(rgb_img, bbox=_bbox, landmark = _landmark, image_size='200,200')
        bgr = warped[...,::-1]
        print(savepath)
        #print(bgr.shape)
        if savepath.endswith('.png'):
            cv2.imwrite(savepath, bgr, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
        elif savepath.endswith('.jpg'):
            cv2.imwrite(savepath, bgr, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        else:
            raise ValueError("unknown image format")
    
    else:
        print("no face was detected")

# used
def align_faces(bounding_boxes, all_points, rgb_img, image_size='112,112'):
    '''
    discription: 
        input n bounding box and point pairs, and the rgb_image and output size of aligned_faces
        output a n x h x w x 3 face
    bounding_boxes: n x 4 numpy array type: np.float32
    points: n x 10 numpy array type:np.float32
    rgb_img: image frame with bgr channels order
    image_size: 112 x 112, the final output size of image
    '''
    #bounding_boxes, points = mtcnn_detector(rgb_img)

    #_bbox = bounding_boxes[bindex, 0:4]
    #_landmark = points[:, bindex].reshape( (2,5) ).T
    n = bounding_boxes.shape[0]

    image_rgb = rgb_img.copy()

    if n == 0:
        return np.zeros((0,160,160,3),dtype=np.float32)

    aligned_faces = np.zeros((n,160,160,3),dtype=np.float32)
    
    for idx in range(n):
        face_box = bounding_boxes[idx,:4]
        face_points = all_points[:10,idx].reshape( (2,5) ).T
        align_face_rgb = face_preprocess.preprocess(image_rgb, bbox=face_box, landmark = face_points, image_size=image_size)
        aligned_faces[idx,:,:,:] = np.asarray(align_face_rgb, dtype=np.float32)


    # bgr = warped[...,::-1]
    return aligned_faces


def get_alignface(bounding_boxes, points, rgb_img, image_size='112,112'):

    #bounding_boxes, points = mtcnn_detector(rgb_img)

    #_bbox = bounding_boxes[bindex, 0:4]
    #_landmark = points[:, bindex].reshape( (2,5) ).T
    align_face_rgb = face_preprocess.preprocess(rgb_img.copy(), bbox=bounding_boxes, landmark = points.reshape( (2,5) ).T, image_size=image_size)
    # bgr = warped[...,::-1]
    return align_face_rgb


def get_alignface_batch(bounding_boxes, points, rgb_img):

    #bounding_boxes, points = mtcnn_detector(rgb_img)
    nrof_faces = bounding_boxes.shape[0]
    _bbox = None
    _landmark = None
    if nrof_faces>0:
        det = bounding_boxes[:,0:4]
        img_size = np.asarray(rgb_img.shape)[0:2]
        bindex = 0
        if nrof_faces>1:
            # following is to find the face in the middle if multi face detected in one image
            bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
            img_center = img_size / 2
            offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
            offset_dist_squared = np.sum(np.power(offsets,2.0),0)
            bindex = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
        _bbox = bounding_boxes[bindex, 0:4]
        _landmark = points[:, bindex].reshape( (2,5) ).T


        warped = face_preprocess.preprocess(rgb_img, bbox=_bbox, landmark = _landmark, image_size='112,112')
        bgr = warped[...,::-1]
        #print(bgr.shape)
        if savepath.endswith('.png'):
            cv2.imwrite(savepath, bgr, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
        elif savepath.endswith('.jpg'):
            cv2.imwrite(savepath, bgr, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        else:
            raise ValueError("unknown image format")
    
    else:
        print("no face was detected")









'''

    #with open(output_filename, "w") as text_file:
    nrof_images_total = 0
    nrof = np.zeros( (5,), dtype=np.int32)
    for fimage in dataset:

        if nrof_images_total%100==0:
            print("Processing %d, (%s)" % (nrof_images_total, nrof))
        nrof_images_total += 1
        #if nrof_images_total<950000:
        #  continue
        image_path = fimage.image_path
        if not os.path.exists(image_path):
            print('image not found (%s)'%image_path)
            continue
        filename = os.path.splitext(os.path.split(image_path)[1])[0]
        #print(image_path)
        try:
            img = misc.imread(image_path)
        except (IOError, ValueError, IndexError) as e:
            errorMessage = '{}: {}'.format(image_path, e)
            print(errorMessage)
        else:
            if img.ndim<2:
                print('Unable to align "%s", img dim error' % image_path)
                #text_file.write('%s\n' % (output_filename))
                continue
            if img.ndim == 2:
                img = to_rgb(img)
            img = img[:,:,0:3]
            _paths = fimage.image_path.split('/')
            a,b = _paths[-2], _paths[-1]
            target_dir = os.path.join(args.output_dir, a)
            if not os.path.exists(target_dir):
                os.makedirs(target_dir)
            target_file = os.path.join(target_dir, b)

            if os.path.exists(target_file):
                # target exit, continune from break
                continue
            _minsize = minsize
            _bbox = None
            _landmark = None
            bounding_boxes, points = detect_face.detect_face(img, _minsize, pnet, rnet, onet, threshold, factor)
            nrof_faces = bounding_boxes.shape[0]
            if nrof_faces>0:
                det = bounding_boxes[:,0:4]
                img_size = np.asarray(img.shape)[0:2]
                bindex = 0
                if nrof_faces>1:
                    bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
                    img_center = img_size / 2
                    offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
                    offset_dist_squared = np.sum(np.power(offsets,2.0),0)
                    bindex = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
                _bbox = bounding_boxes[bindex, 0:4]
                _landmark = points[:, bindex].reshape( (2,5) ).T
                nrof[0]+=1
            else:
                print(image_path)
                nrof[1]+=1
                no_detect_list.append(image_path)
            warped = face_preprocess.preprocess(img, bbox=_bbox, landmark = _landmark, image_size=args.image_size)
            bgr = warped[...,::-1]
            #print(bgr.shape)
            if target_file.endswith('.png'):
                cv2.imwrite(target_file, bgr, [int(cv2.IMWRITE_PNG_COMPRESSION), 9])
            else:
                cv2.imwrite(target_file, bgr, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

            #misc.imsave(target_file,warped)
            #oline = '%d\t%s\t%d\n' % (1,target_file, int(fimage.classname))
            #text_file.write(oline)

    # write down no detect image
    if not os.path.exists("no_detect_img"):
        os.mkdir("no_detect_img")
    with open(output_no_detect_filename, "w") as text_file:
        for item in no_detect_list:
            newpath = os.path.join("no_detect_img", os.path.basename(item))
            shutil.copy(item, newpath)
            text_file.write("%s\n" % item)


'''










def main(args):
    sleep(random.random())
    output_dir = os.path.expanduser(args.output_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    # Store some git revision info in a text file in the log directory
    src_path,_ = os.path.split(os.path.realpath(__file__))
    facenet.store_revision_info(src_path, output_dir, ' '.join(sys.argv))
    dataset = facenet.get_dataset(args.input_dir)
    
    print('Creating networks and loading parameters')
    
    with tf.Graph().as_default():
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=args.gpu_memory_fraction)
        sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options, log_device_placement=False))
        with sess.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(sess, None)
    
    minsize = 20 # minimum size of face
    threshold = [ 0.6, 0.7, 0.7 ]  # three steps's threshold
    factor = 0.709 # scale factor

    # Add a random key to the filename to allow alignment using multiple processes
    random_key = np.random.randint(0, high=99999)
    bounding_boxes_filename = os.path.join(output_dir, 'bounding_boxes_%05d.txt' % random_key)
    
    with open(bounding_boxes_filename, "w") as text_file:
        nrof_images_total = 0
        nrof_successfully_aligned = 0
        if args.random_order:
            random.shuffle(dataset)
        for cls in dataset:
            output_class_dir = os.path.join(output_dir, cls.name)
            if not os.path.exists(output_class_dir):
                os.makedirs(output_class_dir)
                if args.random_order:
                    random.shuffle(cls.image_paths)
            for image_path in cls.image_paths:
                nrof_images_total += 1
                filename = os.path.splitext(os.path.split(image_path)[1])[0]
                output_filename = os.path.join(output_class_dir, filename+'.png')
                print(image_path)
                if not os.path.exists(output_filename):
                    try:
                        img = misc.imread(image_path)
                    except (IOError, ValueError, IndexError) as e:
                        errorMessage = '{}: {}'.format(image_path, e)
                        print(errorMessage)
                    else:
                        if img.ndim<2:
                            print('Unable to align "%s"' % image_path)
                            text_file.write('%s\n' % (output_filename))
                            continue
                        if img.ndim == 2:
                            img = facenet.to_rgb(img)
                        img = img[:,:,0:3]
    
                        bounding_boxes, _ = detect_face.detect_face(img, minsize, pnet, rnet, onet, threshold, factor)
                        nrof_faces = bounding_boxes.shape[0]
                        if nrof_faces>0:
                            det = bounding_boxes[:,0:4]
                            det_arr = []
                            img_size = np.asarray(img.shape)[0:2]
                            if nrof_faces>1:
                                if args.detect_multiple_faces:
                                    for i in range(nrof_faces):
                                        det_arr.append(np.squeeze(det[i]))
                                else:
                                    bounding_box_size = (det[:,2]-det[:,0])*(det[:,3]-det[:,1])
                                    img_center = img_size / 2
                                    offsets = np.vstack([ (det[:,0]+det[:,2])/2-img_center[1], (det[:,1]+det[:,3])/2-img_center[0] ])
                                    offset_dist_squared = np.sum(np.power(offsets,2.0),0)
                                    index = np.argmax(bounding_box_size-offset_dist_squared*2.0) # some extra weight on the centering
                                    det_arr.append(det[index,:])
                            else:
                                det_arr.append(np.squeeze(det))

                            for i, det in enumerate(det_arr):
                                det = np.squeeze(det)
                                bb = np.zeros(4, dtype=np.int32)
                                bb[0] = np.maximum(det[0]-args.margin/2, 0)
                                bb[1] = np.maximum(det[1]-args.margin/2, 0)
                                bb[2] = np.minimum(det[2]+args.margin/2, img_size[1])
                                bb[3] = np.minimum(det[3]+args.margin/2, img_size[0])
                                cropped = img[bb[1]:bb[3],bb[0]:bb[2],:]
                                scaled = misc.imresize(cropped, (args.image_size, args.image_size), interp='bilinear')
                                nrof_successfully_aligned += 1
                                filename_base, file_extension = os.path.splitext(output_filename)
                                if args.detect_multiple_faces:
                                    output_filename_n = "{}_{}{}".format(filename_base, i, file_extension)
                                else:
                                    output_filename_n = "{}{}".format(filename_base, file_extension)
                                misc.imsave(output_filename_n, scaled)
                                text_file.write('%s %d %d %d %d\n' % (output_filename_n, bb[0], bb[1], bb[2], bb[3]))
                        else:
                            print('Unable to align "%s"' % image_path)
                            text_file.write('%s\n' % (output_filename))
                            
    print('Total number of images: %d' % nrof_images_total)
    print('Number of successfully aligned images: %d' % nrof_successfully_aligned)
            

def parse_arguments(argv):
    parser = argparse.ArgumentParser()
    
    parser.add_argument('input_dir', type=str, help='Directory with unaligned images.')
    parser.add_argument('output_dir', type=str, help='Directory with aligned face thumbnails.')
    parser.add_argument('--image_size', type=int,
        help='Image size (height, width) in pixels.', default=182)
    parser.add_argument('--margin', type=int,
        help='Margin for the crop around the bounding box (height, width) in pixels.', default=44)
    parser.add_argument('--random_order', 
        help='Shuffles the order of images to enable alignment using multiple processes.', action='store_true')
    parser.add_argument('--gpu_memory_fraction', type=float,
        help='Upper bound on the amount of GPU memory that will be used by the process.', default=1.0)
    parser.add_argument('--detect_multiple_faces', type=bool,
                        help='Detect and align multiple faces per image.', default=False)
    return parser.parse_args(argv)

if __name__ == '__main__':
    main(parse_arguments(sys.argv[1:]))
