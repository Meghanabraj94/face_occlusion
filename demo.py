from keras.models import Sequential, Model
from keras.layers import *
from keras.layers.advanced_activations import LeakyReLU
from keras.activations import relu
from keras.initializers import RandomNormal
from keras.applications import *
import keras.backend as K

import os
import time
import cv2
from scipy.misc import imsave
from FCN8s_keras import FCN

import os, math, random
from os.path import *
import numpy as np
from glob import glob
import argparse

import mtcnn as mtcnn

def vgg_preprocess(im):
    im = cv2.resize(im, (500, 500))
    in_ = np.array(im, dtype=np.float32)
    in_ = in_[:,:,::-1]
    in_ -= np.array((104.00698793,116.66876762,122.67891434))
    in_ = in_[np.newaxis,:]
    #in_ = in_.transpose((2,0,1))
    return in_
  
def auto_downscaling(im):
    w = im.shape[1]
    h = im.shape[0]
    while w*h >= 700*700:
        im = cv2.resize(im, (0,0), fx=0.5, fy=0.5)
        w = im.shape[1]
        h = im.shape[0]
    return im

def get_args():
    parser = argparse.ArgumentParser(description="This script detects faces "
                                                 "and estimates occlusion score for the face.",
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--root", type=str, default=" ",
                        help="images_folder")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    root = args.root

    types = ('*.png', '*.jpg', '*.jpeg') 
    file_list = []
    for files in types:
        file_list.extend(sorted(glob(join(root, files))))

    model = FCN()
    model.load_weights("Keras_FCN8s_face_seg_YuvalNirkin.h5")

    detector = mtcnn.create_mtcnn()

    for file in file_list:

        im = cv2.cvtColor(cv2.imread(file), cv2.COLOR_BGR2RGB)
        org_im = im.copy()
        H,W,_ = im.shape
        mask = np.zeros((H,W))
        
        #face_detection
        bounding_boxes,points = detector(im)
        num_faces = bounding_boxes.shape[0]
        if (num_faces < 1):
            print(" No face is detected")
            continue

        bbox = bounding_boxes[0]
        landmarks = points[:10,0].reshape( (2,5) ).T
        face = im[int(bbox[1]):int(bbox[3]),int(bbox[0]):int(bbox[2]),:]

        #face_segmentation
        im_seg = auto_downscaling(face)
        inp_im = vgg_preprocess(im_seg)
        out = model.predict([inp_im])
        out_resized = cv2.resize(np.squeeze(out), (face.shape[1],face.shape[0]))
        out_resized_clipped = np.clip(out_resized.argmax(axis=2), 0, 1).astype(np.float64)

        #area computation
        H,W,_ = face.shape
        image_area = np.ones((H,W))
        bbox_area = image_area.sum() #(image_area[int(bbox[1]):int(bbox[3]),int(bbox[0]):int(bbox[2])]).sum()
        seg_area = out_resized_clipped.sum()#(out_resized_clipped[int(bbox[1]):int(bbox[3]),int(bbox[0]):int(bbox[2])]).sum()
        occ = 1 - (seg_area/bbox_area)

        mask[int(bbox[1]):int(bbox[3]),int(bbox[0]):int(bbox[2])] = out_resized_clipped


        filename = root + "/" + (file[len(root)+1:]).split(".")[0] + "_mask.png"
        imsave(filename, mask)
        mask = cv2.imread(filename,1)

        #drawing points and landmarks

        cv2.putText(mask,str(occ),(int(bbox[0]),int(bbox[1])),cv2.FONT_HERSHEY_TRIPLEX,1,color=(0,0,255))
        cv2.rectangle(org_im, (int(bbox[0]),int(bbox[1])),(int(bbox[2]),int(bbox[3])),(255,0,0))
        cv2.rectangle(mask, (int(bbox[0]),int(bbox[1])),(int(bbox[2]),int(bbox[3])),(0,0,255))
        for j in range(5):
            cv2.circle(org_im, (landmarks[j,0], landmarks[j,1]),3,(255,0,0))
        im = cv2.cvtColor(org_im, cv2.COLOR_RGB2BGR)
        images = np.concatenate((im, mask), axis=1)
        cv2.imwrite(filename, images)

if __name__ == '__main__':
    main()