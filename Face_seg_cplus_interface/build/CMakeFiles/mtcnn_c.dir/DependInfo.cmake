# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/meghana/code/MTCNN/face_seg.cpp" "/home/meghana/code/MTCNN/build/CMakeFiles/mtcnn_c.dir/face_seg.cpp.o"
  "/home/meghana/code/MTCNN/main.cpp" "/home/meghana/code/MTCNN/build/CMakeFiles/mtcnn_c.dir/main.cpp.o"
  "/home/meghana/code/MTCNN/utilities.cpp" "/home/meghana/code/MTCNN/build/CMakeFiles/mtcnn_c.dir/utilities.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "/usr/local/cuda/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
